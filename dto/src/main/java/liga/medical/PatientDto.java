package liga.medical;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PatientDto {

    private String name;

    private String gender;

    private Integer age;

    private String city;

    private String address;

    private LocalDate birthdayDt;

    private String birthPlace;

    private String registration;

    private Integer passportSeries;

    private Integer passportNumber;

    private String phoneNumber;

    private String anotherDocument;
}
